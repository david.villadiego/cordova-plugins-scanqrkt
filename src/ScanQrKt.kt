package cordova.plugin

import android.content.Intent
import com.google.zxing.integration.android.IntentIntegrator
import org.apache.cordova.CallbackContext
import org.apache.cordova.CordovaActivity
import org.apache.cordova.CordovaPlugin
import org.json.JSONArray
import org.json.JSONException
import java.util.*


class ScanQrKt : CordovaPlugin() {
    private var scanCallbackContext: CallbackContext? = null

    override fun execute(
        action: String,
        args: JSONArray,
        callbackContext: CallbackContext
    ): Boolean {
        // Verify that the user sent a 'scan' action
        if (action != "scanqrkt") {
            callbackContext.error("\"$action\" is not a recognized action.")
            return false
        }

        // Creating new interface (Integrator)
        val integrator =
            IntentIntegrator(cordova.activity as CordovaActivity)
        cordova.setActivityResultCallback(this)
        try {
            val params = args.getJSONObject(0)
            if (params.has("prompt_message") && params.getString("prompt_message").length > 0) integrator.setPrompt(
                params.getString("prompt_message")
            ) // Prompt Message
            if (params.has("orientation_locked")) integrator.setOrientationLocked(
                params.getBoolean(
                    "orientation_locked"
                )
            ) // Orientation Locked
            if (params.has("camera_id")) integrator.setCameraId(params.getInt("camera_id")) // Camera Id
            if (params.has("beep_enabled")) integrator.setBeepEnabled(params.getBoolean("beep_enabled")) // Beep Enabled
            if (params.has("timeout")) integrator.setTimeout(
                params.getInt("timeout").toLong()
            ) // Timeout

            // Scan Type
            if (params.has("scan_type")) {
                val scanType = params.getString("scan_type")
                if (scanType == "inverted") integrator.addExtra(
                    "SCAN_TYPE",
                    1
                ) else if (scanType == "mixed") integrator.addExtra(
                    "SCAN_TYPE",
                    2
                ) else integrator.addExtra("SCAN_TYPE", 0)
            }

            // Barcode Formats
            if (params.has("barcode_formats")) {
                val formats =
                    ArrayList<String>()
                val barcodeFormats =
                    params.getJSONArray("barcode_formats") as JSONArray
                if (barcodeFormats != null) {
                    for (i in 0 until barcodeFormats.length()) {
                        formats.add(barcodeFormats.getString(i))
                    }
                    if (!formats.isEmpty()) integrator.setDesiredBarcodeFormats(formats)
                }
            }

            // Extras
            val extras =
                if (params.has("extras")) params.getJSONObject("extras") else null
            if (extras != null) {
                val extraNames = extras.names()
                if (extraNames != null) {
                    for (i in 0 until extraNames.length()) {
                        val key = extraNames.getString(i)
                        val value = extras[key]
                        integrator.addExtra(key, value)
                    }
                }
            }
        } catch (e: JSONException) {
            callbackContext.error("Error encountered: " + e.message)
            return false
        }

        // Init scanner using a camera
        integrator.initiateScan()
        scanCallbackContext = callbackContext
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        val result =
            IntentIntegrator.parseActivityResult(requestCode, resultCode, intent)
        if (result != null) {
            if (result.contents == null) {
                scanCallbackContext!!.error("cancelled")
            } else {
                scanCallbackContext!!.success(result.contents)
            }
        } else {
            // This is important, otherwise the result will not be passed to the fragment
            super.onActivityResult(requestCode, resultCode, intent)
        }
    }
}
