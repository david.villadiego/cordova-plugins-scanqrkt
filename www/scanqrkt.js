/*global cordova, module*/

module.exports = {
    scan: function (params, successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "ScanQrKt", "scanqrkt", [params]);
    }
};
